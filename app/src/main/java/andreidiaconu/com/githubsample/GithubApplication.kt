package andreidiaconu.com.githubsample

import android.app.Activity
import android.app.Application

class GithubApplication: Application() {
    companion object {
        val injection = Injection()
    }
}

fun Activity.injection() = GithubApplication.injection
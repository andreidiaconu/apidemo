package andreidiaconu.com.githubsample

import andreidiaconu.com.githubsample.api.APIConfiguration
import andreidiaconu.com.githubsample.api.RetrofitFactory
import andreidiaconu.com.githubsample.browse.BrowseContract
import andreidiaconu.com.githubsample.browse.BrowsePresenter
import andreidiaconu.com.githubsample.details.DetailsContract
import andreidiaconu.com.githubsample.details.DetailsPresenter
import andreidiaconu.com.githubsample.interactor.GithubServiceInteractor
import andreidiaconu.com.githubsample.interactor.GithubServiceInteractorImpl
import retrofit2.Retrofit

/**
 * This is a very naive implementation for dependency injection. We should configure Dagger moving forward
 */
class Injection{

    private var scopedBrowsePresenter: BrowseContract.Presenter? = null
    private var scopedDetailsPresenter: DetailsContract.Presenter? = null

    /**
     * Among other solutions, we could rely on Dagger to scope the presenter to live as long
     * as the activity it is used by (including between recreation events)
     */
    fun provideBrowsePresenter(): BrowseContract.Presenter {
        if (scopedBrowsePresenter==null) {
            scopedBrowsePresenter = BrowsePresenter(provideGithubServiceInteractor())
        }
        return scopedBrowsePresenter!!
    }

    /**
     * Among other solutions, we could rely on Dagger to scope the presenter to live as long
     * as the activity it is used by (including between recreation events)
     */
    fun provideDetailsPresenter(): DetailsContract.Presenter {
        if (scopedDetailsPresenter==null) {
            scopedDetailsPresenter = DetailsPresenter(provideGithubServiceInteractor())
        }
        return scopedDetailsPresenter!!
    }

    fun provideGithubServiceInteractor(): GithubServiceInteractor = GithubServiceInteractorImpl(provideRetrofit())

    fun provideRetrofit(): Retrofit = RetrofitFactory(provideAPIConfiguration()).build()

    fun provideAPIConfiguration(): APIConfiguration = APIConfiguration("https://api.github.com")
}
package andreidiaconu.com.githubsample.api

import andreidiaconu.com.githubsample.model.ListWrapper
import andreidiaconu.com.githubsample.model.Repository
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {
    companion object {
        // could also be enums
        val SORT_STARS = "stars"
        val SORT_FORKS = "forks"
        val SORT_UPDATED = "updated"
    }

    @GET("search/repositories")
    fun searchRepositories(@Query("q") query: String, @Query("sort") sort: String, @Query("page") page: Int): Single<ListWrapper<Repository>>

    @GET("repos/{owner}/{repo}")
    fun getRepositoryDetails(@Path("owner") owner: String, @Path("repo") repo: String): Single<Repository>
}
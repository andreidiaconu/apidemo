package andreidiaconu.com.githubsample.api

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

class RetrofitFactory(val configuration: APIConfiguration) {

    fun build(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(configuration.baseUrl)
                .addConverterFactory(
                        JacksonConverterFactory.create(
                                jacksonObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}
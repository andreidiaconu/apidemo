package andreidiaconu.com.githubsample.browse

import andreidiaconu.com.githubsample.model.Repository

interface BrowseContract {
    interface Presenter {
        fun attachView(view: View)
        fun requestNewPage()
        fun itemSelected(position: Int)
        fun detachView()
    }

    interface View {
        fun updateRepositories(repos: List<Repository>)
        fun openRepositoryDetails(repository: Repository)
        fun showApiError()
    }
}
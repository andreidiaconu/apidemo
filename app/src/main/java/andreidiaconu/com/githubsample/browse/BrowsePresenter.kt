package andreidiaconu.com.githubsample.browse

import andreidiaconu.com.githubsample.interactor.GithubServiceInteractor
import andreidiaconu.com.githubsample.model.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class BrowsePresenter(val githubInteractor: GithubServiceInteractor) : BrowseContract.Presenter {
    var view: BrowseContract.View? = null
    var repositories: MutableList<Repository> = mutableListOf()
    var lastSuccessfulPage = 0
    var lastRequest: Disposable? = null

    override fun attachView(view: BrowseContract.View) {
        this.view = view
        updateView()
        requestNewPage()
    }

    private fun updateView() {
        view?.updateRepositories(repositories)
    }

    override fun requestNewPage() {
        lastRequest.let {
            if (it == null || it.isDisposed) {
                lastRequest = githubInteractor.getTrendingRepositories("android", lastSuccessfulPage + 1)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            lastSuccessfulPage++
                            repositories.addAll(it)
                            updateView()
                        }, {
                            view?.showApiError()
                        })
            }
        }
    }

    override fun itemSelected(position: Int) {
        view?.let { it.openRepositoryDetails(repositories[position]) }
    }

    override fun detachView() {
        this.view = null
    }

}
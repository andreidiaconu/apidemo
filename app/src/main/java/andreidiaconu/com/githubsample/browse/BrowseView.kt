package andreidiaconu.com.githubsample.browse

import andreidiaconu.com.githubsample.R
import andreidiaconu.com.githubsample.browse.adapters.LoadingWrapperAdapter
import andreidiaconu.com.githubsample.browse.adapters.RepositoryAdapter
import andreidiaconu.com.githubsample.details.DetailsView
import andreidiaconu.com.githubsample.injection
import andreidiaconu.com.githubsample.model.Repository
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_browse.*

class BrowseView : AppCompatActivity(), BrowseContract.View {

    private val presenter = injection().provideBrowsePresenter()
    private val adapter = RepositoryAdapter({ presenter.itemSelected(it) })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browse)
        repositoryList.layoutManager = LinearLayoutManager(this)
        repositoryList.adapter = LoadingWrapperAdapter(adapter)
        presenter.attachView(this)
        setupListPagination()
    }

    private fun setupListPagination() {
        val layoutManager = LinearLayoutManager(this)
        repositoryList.layoutManager = layoutManager
        repositoryList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisible = layoutManager.findLastVisibleItemPosition()
                if (adapter.items.size < lastVisible + 4) { //We are at the end of the list
                    presenter.requestNewPage()
                }
            }
        })
    }

    override fun updateRepositories(repos: List<Repository>) {
        adapter.items = repos
    }

    override fun openRepositoryDetails(repository: Repository) {
        DetailsView.open(this, repository)
    }

    override fun showApiError() {
        Toast.makeText(this, "There was an error somewhere", Toast.LENGTH_LONG).show()
    }


}

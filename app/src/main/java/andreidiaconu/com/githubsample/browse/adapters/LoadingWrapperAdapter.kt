package andreidiaconu.com.githubsample.browse.adapters

import andreidiaconu.com.githubsample.R
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class LoadingWrapperAdapter<D: RecyclerView.ViewHolder>(private val innerAdapter: RecyclerView.Adapter<D>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        val TYPE_LOADER = 12345
    }

    init {
        innerAdapter.registerAdapterDataObserver(object: RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                notifyDataSetChanged()
            }
        })
    }

    override fun getItemCount() = innerAdapter.itemCount + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) != TYPE_LOADER) {
            innerAdapter.onBindViewHolder(holder as D, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < innerAdapter.itemCount) innerAdapter.getItemViewType(position) else TYPE_LOADER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            (TYPE_LOADER) -> LoaderVH(LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false))
            else -> innerAdapter.onCreateViewHolder(parent, viewType)
        }
    }

    class LoaderVH(view: View): RecyclerView.ViewHolder(view)
}
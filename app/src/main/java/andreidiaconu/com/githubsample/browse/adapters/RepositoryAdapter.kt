package andreidiaconu.com.githubsample.browse.adapters

import andreidiaconu.com.githubsample.R
import andreidiaconu.com.githubsample.model.Repository
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryAdapter(private val onSelectedListener: (Int) -> Unit) : RecyclerView.Adapter<RepositoryAdapter.RepositoryVH>() {
    var items: List<Repository> = listOf()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RepositoryVH, position: Int) {
        val repository = items[position]
        holder.title.text = repository.name
        holder.owner.text = holder.owner.context.getString(R.string.by_x, repository.owner.login)
    }

    override fun getItemCount()= items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false)
        return RepositoryVH(view, onSelectedListener)
    }


    class RepositoryVH(val view: View, onSelectedListener: (Int) -> Unit): RecyclerView.ViewHolder(view) {
        val title = view.repositoryTitle
        val owner = view.repositoryOwner
        init {
            view.repositoryCard.setOnClickListener { onSelectedListener(adapterPosition) }
        }
    }
}
package andreidiaconu.com.githubsample.details

import andreidiaconu.com.githubsample.model.Repository

interface DetailsContract{
    interface Presenter {
        fun attachView(view: DetailsContract.View)
        fun setCache(cache: Repository)
        fun identifyRepository(owner: String, repo: String)
        fun openBrowserClicked()
        fun detachView()
    }

    interface View {
        fun updateRepository(repo: Repository)
        fun showApiError()
        fun openBrowserAtUrl(url: String)
    }
}
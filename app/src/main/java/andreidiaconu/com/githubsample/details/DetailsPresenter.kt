package andreidiaconu.com.githubsample.details

import andreidiaconu.com.githubsample.interactor.GithubServiceInteractor
import andreidiaconu.com.githubsample.model.Repository
import io.reactivex.android.schedulers.AndroidSchedulers

class DetailsPresenter(val githubInteractor: GithubServiceInteractor) : DetailsContract.Presenter {
    var view: DetailsContract.View? = null
    var cacheRepository: Repository? = null
    var freshRepository: Repository? = null

    override fun attachView(view: DetailsContract.View) {
        this.view = view
        updateView()
    }

    private fun updateView() {
        getFreshestData()?.let { view?.updateRepository(it) }
    }

    private fun getFreshestData(): Repository?{
        return freshRepository ?: cacheRepository
    }

    override fun setCache(cache: Repository) {
        cacheRepository = cache
    }

    override fun identifyRepository(owner: String, repo: String) {
        githubInteractor.getRepository(owner, repo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    freshRepository = it
                    updateView()
                }, {
                    view?.showApiError()
                })
    }

    override fun openBrowserClicked() {
        getFreshestData()?.let { view?.openBrowserAtUrl(it.publicUrl) }
    }

    override fun detachView() {
        view = null
    }

}
package andreidiaconu.com.githubsample.details

import andreidiaconu.com.githubsample.R
import andreidiaconu.com.githubsample.injection
import andreidiaconu.com.githubsample.model.Repository
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_details.*

class DetailsView : AppCompatActivity(), DetailsContract.View {

    companion object {
        val REPOSITORY_PARCEL = "repository"
        val OWNER = "owner"
        val REPOSITORY_NAME = "repository_name"
        fun open(from: Activity, repository: Repository) {
            val intent = Intent(from, DetailsView::class.java)
            intent.putExtra(REPOSITORY_PARCEL, repository)
            intent.putExtra(OWNER, repository.owner.login)
            intent.putExtra(REPOSITORY_NAME, repository.name)
            from.startActivity(intent)
        }
    }

    private val presenter = injection().provideDetailsPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        presenter.attachView(this)
        passDataToPresenter()
        openBrowser.setOnClickListener { presenter.openBrowserClicked() }
    }

    private fun passDataToPresenter() {
        val cache = intent.getParcelableExtra<Repository>(REPOSITORY_PARCEL)
        val owner = intent.getStringExtra(OWNER)
        val repositoryName = intent.getStringExtra(REPOSITORY_NAME)
        presenter.setCache(cache)
        presenter.identifyRepository(owner, repositoryName)
    }

    override fun updateRepository(repo: Repository) {
        repositoryTitle.text = repo.name
        repositoryOwner.text = getString(R.string.by_x, repo.owner.login)
        repositoryDescription.text = repo.description
        repositoryStars.text = getString(R.string.x_stars,repo.stars)
        repositoryWatchers.text = getString(R.string.x_watchers,repo.watchers)
        repositoryForks.text = getString(R.string.x_forks,repo.forks)
    }

    override fun openBrowserAtUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    override fun showApiError() {
        Toast.makeText(this, "There was an error somewhere", Toast.LENGTH_LONG).show()
    }
}
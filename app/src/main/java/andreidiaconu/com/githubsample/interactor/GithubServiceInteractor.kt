package andreidiaconu.com.githubsample.interactor

import andreidiaconu.com.githubsample.model.Repository
import io.reactivex.Single

interface GithubServiceInteractor {
    fun getTrendingRepositories(query: String, page: Int): Single<List<Repository>>
    fun getRepository(owner: String, repo: String): Single<Repository>
}
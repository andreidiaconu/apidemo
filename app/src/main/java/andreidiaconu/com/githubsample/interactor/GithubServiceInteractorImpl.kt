package andreidiaconu.com.githubsample.interactor

import andreidiaconu.com.githubsample.api.GithubService
import andreidiaconu.com.githubsample.model.Repository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

class GithubServiceInteractorImpl(val retrofit: Retrofit): GithubServiceInteractor {

    val service = retrofit.create(GithubService::class.java)

    override fun getTrendingRepositories(query: String, page: Int): Single<List<Repository>> =
        service.searchRepositories(query, GithubService.SORT_STARS, page)
                .subscribeOn(Schedulers.io())
                .map { it.items }

    override fun getRepository(owner: String, repo: String): Single<Repository> =
            service.getRepositoryDetails(owner, repo)
                    .subscribeOn(Schedulers.io())

}
package andreidiaconu.com.githubsample.model

import com.fasterxml.jackson.annotation.JsonProperty

data class ListWrapper<out D>(@JsonProperty("total_count") val totalCount: Long, val items: List<D>)
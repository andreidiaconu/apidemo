package andreidiaconu.com.githubsample.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class Owner(val id: Long,
                 val login: String,
                 @JsonProperty("avatar_url") val avatarUrl: String) : Parcelable
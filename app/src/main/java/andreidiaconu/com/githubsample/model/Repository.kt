package andreidiaconu.com.githubsample.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class Repository(val id: Long,
                      val name: String,
                      val owner: Owner,
                      val description: String?,
                      @JsonProperty("full_name") val fullName: String,
                      @JsonProperty("stargazers_count") val stars: Int,
                      @JsonProperty("watchers_count") val watchers: Int,
                      @JsonProperty("forks_count") val forks: Int,
                      @JsonProperty("html_url") val publicUrl: String) : Parcelable
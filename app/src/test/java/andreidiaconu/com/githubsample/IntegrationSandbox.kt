package andreidiaconu.com.githubsample

import andreidiaconu.com.githubsample.api.GithubService
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory


/**
 * Testing integrations here while developing
 */
class IntegrationSandbox {
    @Test
    fun retrofitConfig() {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(
                        JacksonConverterFactory.create(
                                jacksonObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val service = retrofit.create(GithubService::class.java)
        val listOfReposResponse = service.searchRepositories("android").blockingGet()
        val firstRepo = listOfReposResponse.items.first()
        System.out.println(firstRepo)
        val firstRepoDetails = service.getRepositoryDetails(firstRepo.owner.login, firstRepo.name).blockingGet()
        System.out.println(firstRepoDetails)
    }
}
